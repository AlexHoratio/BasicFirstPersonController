extends KinematicBody

var MOUSESPEED = 0.005
var JUMP_HEIGHT = 20
var SPEED = 25
var GRAVITY = 1
var gravity_force = 0

onready var playerfeet = get_node("playerfeet")
onready var camera = get_node("Camera")

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	set_physics_process(true)
	set_process_input(true)

func _input(event):
	if event is InputEventMouseMotion:
		rotation.y -= event.relative.x*MOUSESPEED

func _process(delta):
	var touching_ground = playerfeet.is_colliding()
	var movement_vector = Vector3(0, 0, 0)
	
	if(not(touching_ground)):
		gravity_force -= GRAVITY*delta
	else:
		gravity_force = 0
		
	if(Input.is_action_pressed("ui_accept") and touching_ground):
		gravity_force += JUMP_HEIGHT*delta
	
	if Input.is_key_pressed(KEY_W):
		movement_vector.z += SPEED*delta
	if Input.is_key_pressed(KEY_S):
		movement_vector.z -= SPEED*delta
	if Input.is_key_pressed(KEY_A):
		movement_vector.x += SPEED*delta
	if Input.is_key_pressed(KEY_D):
		movement_vector.x -= SPEED*delta

	movement_vector = movement_vector.normalized()

	move_and_collide(Vector3(0, gravity_force, 0) + (movement_vector * SPEED * delta).rotated(Vector3(0, 1, 0), rotation.y))
